import React, { useEffect, useState } from "react";
import TaskItem from "../components/TaskItem.jsx";
import { list, deleteData } from "../services/task.services.js";

import "../styles/Task.css"

const Task = () => {
  const [tasks, setTasks] = useState([]);

  //Obtenemos todos los tasks

  const getData = async () => {
    const result = await list();
    console.log(result);
    if (!result.status)
      return alert("Something went wrong! - " + result.message);

    setTasks(result.data);
  };

  //Manejador eliminar
  const handleDelete = async (id, title) => {
    const respuesta = confirm(`Do you want to delete this task "${title}"?`);
    if(!respuesta) return;

    const result = await deleteData(id);
    if(!result.status) return alert("Something went wrong! - " + result.message);

    getData();
  };

  //Al cargar la página, ejecuta el código que tienes en la funcion
  useEffect(() => {
    getData();
  }, []);

  if (tasks === undefined || tasks === null || tasks.length === 0)
    return <div>Loading...</div>

  return (
    <div className="task-container">
      {tasks.map((task) => (
        <TaskItem
          id={task.id}
          title={task.title}
          description={task.description}
          key={task.id}
          hDelete={handleDelete}
        />
      ))}
    </div>
  );
};

export default Task;
