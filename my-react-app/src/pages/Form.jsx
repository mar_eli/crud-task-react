import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { get, create, update } from "../services/task.services";
import "../styles/Form.css";

const Form = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  //Estados
  const [txtTitle, setTxtTitle] = useState("");
  const [txtDescription, setTxtDescription] = useState("");

  //Obtenemos task
  const getTask = async () => {
    const result = await get(id);
    if (!result.status)
      return alert("Something went wrong! - " + result.message);

    setTxtTitle(result.data.title);
    setTxtDescription(result.data.description);
  };

  //Al enviar la info
  const handleSubmit = async (e) => {
    e.preventDefault();

    const task = {
      title: txtTitle,
      description: txtDescription,
    };

    let result;

    if (id) {
      result = await update(id, task);
    } else {
      result = await create(task);
    }
    if (!result.status)
      return alert("Something went wrong! - " + result.message);

    navigate(-1);
  };

  useEffect(() => {
    if (id) getTask();

    return () => {
      setTxtTitle("");
      setTxtDescription("");
    };
  }, []);

  return (
    <div className="form-container">
      <div className="form-title">{id ? "Update" : "Save"} task</div>

      <form className="form" onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Title"
          className="form-input"
          onChange={(e) => setTxtTitle(e.target.value)}
          value={txtTitle}
        />

        <input
          type="text"
          placeholder="Description"
          className="form-input"
          onChange={(e) => setTxtDescription(e.target.value)}
          value={txtDescription}
        />

        <button type="submit" className="form-btn">
          {id ? "Update" : "Save"}
        </button>
      </form>
    </div>
  );
};

export default Form;
