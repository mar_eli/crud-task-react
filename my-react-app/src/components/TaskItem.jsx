import React from 'react'
import { Link } from 'react-router-dom'

const TaskItem = ({ id, title, description, hDelete }) => {
  return (
    <div className="task">
        <div className="task-header">{title}</div>

            <div className="task-body">
                <p>{description}</p>

            <div className="task-actions">
                <Link to={`/edit/${id}`} className="task-actions-btn">Edit</Link>
                <button className="task-actions-btn" onClick={() => hDelete(id, title)}>Delete</button>
            </div>
        </div>
    </div>
  );
};

export default TaskItem;