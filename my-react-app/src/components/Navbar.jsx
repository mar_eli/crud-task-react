import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <header className="header">
      <nav className="nav-main">
        <Link to="/" className="nav-brand">
          JS App
        </Link>

        <Link to="/add">Add task</Link>
      </nav>
    </header>
  );
};

export default Navbar;
